using UnityEngine;

public class TowerPlacement : MonoBehaviour
{
    Tower _placedTower;

    void OnTriggerEnter2D (Collider2D collider) {

        if (_placedTower != null) {
            return;
        }

        Tower tower = collider.GetComponent<Tower>();
        if (tower != null) {
            tower.SetPlacePosition(transform.position);
            _placedTower = tower;
        }
    }

    private void OnTriggerExit2D(Collider2D collider) {
        if (_placedTower == null) {
            return;
        }

        _placedTower.SetPlacePosition(null);
        _placedTower = null;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

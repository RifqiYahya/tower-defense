using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    //Singleton Function
    static LevelManager _instance = null;
    public static LevelManager instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<LevelManager>();
            }

            return _instance;
        }
    }

    [SerializeField] Transform _towerUIParent;
    [SerializeField] GameObject _towerUIPrefab;

    [SerializeField] Tower[] _towerPrefabs;
    [SerializeField] Enemy[] _enemyPrefabs;

    [SerializeField] Transform[] _enemyPaths;
    [SerializeField] float _spawnDelay = 5f;

    List<Tower> _spawnedTowers = new List<Tower>();
    List<Enemy> _spawnedEnemies = new List<Enemy>();
    List<Bullet> _spawnedBullets = new List<Bullet>();

    //Status
    [Header("Status Manager")]
    [SerializeField] int _maxLives = 3;
    [SerializeField] int _totalEnemy = 15;
    [SerializeField] GameObject _panel;
    [SerializeField] Text _statusInfo;
    [SerializeField] Text _livesInfo;
    [SerializeField] Text _totalEnemyInfo;
    [SerializeField] Text _restart;

    int _currentLives;
    int _enemyCounter;

    public bool IsOver {get; private set;}

    float _runningSpawnDelay;

    public Bullet GetBulletFromPool(Bullet prefab) {
        GameObject newBulletObj = _spawnedBullets.Find(
            b => !b.gameObject.activeSelf && b.name.Contains(prefab.name)
        )?.gameObject;

        if(newBulletObj == null) {
            newBulletObj = Instantiate(prefab.gameObject);
        }

        Bullet newBullet = newBulletObj.GetComponent<Bullet>();
        if(!_spawnedBullets.Contains(newBullet)){
            _spawnedBullets.Add(newBullet);
        }

        return newBullet;
    }

    public void ExplodeAt(Vector2 point, float radius, int damage) {
        foreach(Enemy enemy in _spawnedEnemies) {
            if (enemy.gameObject.activeSelf &&
                Vector2.Distance(enemy.transform.position, point) <= radius) {
                enemy.ReduceEnemyHealth(damage);
            }
        }
    }

    void InstantiateAllTowerUI() {
        foreach(Tower tower in _towerPrefabs) {
            GameObject newTowerUIObj = Instantiate(_towerUIPrefab.gameObject, _towerUIParent);
            TowerUI newTowerUI = newTowerUIObj.GetComponent<TowerUI>();

            newTowerUI.SetTowerPrefab(tower);
            newTowerUI.transform.name = tower.name;
        }
    }
    
    public void RegisterSpawnedTower (Tower tower) {
        _spawnedTowers.Add(tower);
    }

    void SpawnEnemy() {
        SetTotalEnemy(--_enemyCounter);
        if (_enemyCounter < 0) {
            bool isAllEnemyDestroyed = 
                _spawnedEnemies.Find(
                    enabled => enabled.gameObject.activeSelf
                ) == null;
            if (isAllEnemyDestroyed) {
                SetGameOver(true);
            }

            return;
        }

        int randomIndex = Random.Range(0, _enemyPrefabs.Length);
        string enemyIndexString = (randomIndex + 1).ToString();

        GameObject newEnemyObj = _spawnedEnemies.Find(e=> !e.gameObject.activeSelf && e.name.Contains(enemyIndexString))?.gameObject;

        if(newEnemyObj == null) {
            newEnemyObj = Instantiate(_enemyPrefabs[randomIndex].gameObject);
        }

        Enemy newEnemy = newEnemyObj.GetComponent<Enemy>();

        if (!_spawnedEnemies.Contains(newEnemy)){
            _spawnedEnemies.Add(newEnemy);
        }

        newEnemy.transform.position = _enemyPaths[0].position;
        newEnemy.SetTargetPosition(_enemyPaths[1].position);
        newEnemy.SetCurrentPathindex(1);
        newEnemy.gameObject.SetActive(true);
    }

    void OnDrawGizmos() {
        for (int i = 0; i < _enemyPaths.Length - 1; i++) {
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(_enemyPaths[i].position, _enemyPaths[i+1].position);
        }
    }

    public void SetCurrentLives(int currentLives) {
        _currentLives = Mathf.Max(currentLives, 0);
        _livesInfo.text = $"Lives : {_currentLives}";
    }

    public void SetTotalEnemy(int totalEnemy) {
        _enemyCounter = totalEnemy;
        _totalEnemyInfo.text = $"Total Enemy : {Mathf.Max(_enemyCounter, 0)}";
    }

    public void SetGameOver(bool isWin) {
        IsOver = true;

        _statusInfo.text = isWin ? "You WIN!" : "You LOSE!";
        _panel.gameObject.SetActive(true);
        _restart.color = Color.white;
    }

    public void ReduceLives(int value) {
        SetCurrentLives(_currentLives - value);
        if (_currentLives <= 0) {
            SetGameOver(false);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        InstantiateAllTowerUI();
        SetCurrentLives(_maxLives);
        SetTotalEnemy(_totalEnemy);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R)) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        if(IsOver) {
            return;
        }

        _runningSpawnDelay -= Time.unscaledDeltaTime;
        if (_runningSpawnDelay <= 0) {
            SpawnEnemy();
            _runningSpawnDelay = _spawnDelay;
        }

        foreach(Tower tower in _spawnedTowers) {
            tower.CheckNearestEnemy (_spawnedEnemies);
            tower.SeekTarget();
            tower.ShootTarget();
        }

        foreach(Enemy enemy in _spawnedEnemies) {
            if (!enemy.gameObject.activeSelf) {
                continue;
            }

            if (Vector2.Distance(enemy.transform.position, enemy.targetPosition) < 0.1f) {
                enemy.SetCurrentPathindex(enemy.currentPathIndex + 1);
                if (enemy.currentPathIndex < _enemyPaths.Length) {
                    enemy.SetTargetPosition(_enemyPaths[enemy.currentPathIndex].position);
                } else {
                    enemy.gameObject.SetActive(false);
                    ReduceLives(1);
                }
            } else {
                enemy.MoveToTarget();
            }
        }
    }
}

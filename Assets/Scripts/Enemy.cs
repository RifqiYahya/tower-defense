using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] int  _maxHealth = 1;
    [SerializeField] float _moveSpeed = 1f;
    [SerializeField] SpriteRenderer _healthBar;
    [SerializeField] SpriteRenderer _healthFill;

    int _currentHealth;
    public Vector3 targetPosition {get; private set;}
    public int currentPathIndex {get; private set;}

    void OnEnable() {
        _currentHealth = _maxHealth;
        _healthFill.size = _healthBar.size;
    }

    public void MoveToTarget() {
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, _moveSpeed * Time.deltaTime);
    }

    public void SetTargetPosition(Vector3 _targetPosition) {
        targetPosition = _targetPosition;
        _healthBar.transform.parent = null;

        //change enemy rotation
        Vector3 distance = targetPosition - transform.position;
        if (Mathf.Abs(distance.y) > Mathf.Abs(distance.x)){
            if (distance.y > 0) {
                transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 90f));
            } else {
                transform.rotation = Quaternion.Euler(new Vector3(0f,0f,-90f));
            }
        } else {
            if (distance.x >0) {
                transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
            } else {
                transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 180f));
            }
        }

        _healthBar.transform.parent = transform;
    }

    //setting current pth index
    public void SetCurrentPathindex(int currentIndex) {
        currentPathIndex = currentIndex;
    }

    public void ReduceEnemyHealth (int damage) {
        _currentHealth -= damage;
        _healthFill.size = new Vector2(_healthFill.size.x*_currentHealth/_maxHealth, _healthFill.size.y);
        AudioPlayer.instance.PlaySFX("hit-enemy");

        if (_currentHealth <= 0) {
            gameObject.SetActive(false);
            AudioPlayer.instance.PlaySFX("enemy-die");
        }
    }

}

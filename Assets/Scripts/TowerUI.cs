using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class TowerUI : MonoBehaviour,IBeginDragHandler,IDragHandler,IEndDragHandler
{
    [SerializeField] Image _towerIcon;

    Tower _towerPrefab;
    private Tower _currentSpawnedTower;

    public void SetTowerPrefab(Tower tower) {
        _towerPrefab = tower;
        _towerIcon.sprite = _towerPrefab.GetTowerHeadIcon();
    }

    //Begin dragging UI (IBeginDragHandler)
    public void OnBeginDrag(PointerEventData eventData) {
        GameObject newTowerObj = Instantiate(_towerPrefab.gameObject);
        _currentSpawnedTower = newTowerObj.GetComponent<Tower>();
        _currentSpawnedTower.ToggleOrderInLayer(true);
    }

    //Dragging UI (IDragHandler)
    public void OnDrag(PointerEventData eventData) {
        Camera mainCamera = Camera.main;
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = -mainCamera.transform.position.z;
        Vector3 targetPosition = Camera.main.ScreenToWorldPoint(mousePosition);

        _currentSpawnedTower.transform.position = targetPosition;
    }

    //End dragging UI (IEndDragHandler)
    public void OnEndDrag(PointerEventData eventData) {
        if(_currentSpawnedTower.PlacePosition == null) {
            Destroy(_currentSpawnedTower.gameObject);
        } else {
            _currentSpawnedTower.LockPlacement();
            _currentSpawnedTower.ToggleOrderInLayer(false);
            LevelManager.instance.RegisterSpawnedTower(_currentSpawnedTower);
            _currentSpawnedTower = null;
        }
    }
}
